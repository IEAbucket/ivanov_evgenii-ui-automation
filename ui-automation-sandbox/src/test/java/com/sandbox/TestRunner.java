package com.sandbox;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Runner class that uses the Cucumber JUnit runner
 * The @CucumberOptions can be used to provide additional configuration to the runner.
 */
@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber"})
public class TestRunner {
}