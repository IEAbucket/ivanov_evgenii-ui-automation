Feature: Dresses page contains facets with all attributes:
  brand, color, occasion, material, dress length, dress types, neckline style, attractability, theme.


  @mineClassTest
  Scenario: To verify dresses page has all attributes
    Given user opens Home page
    Then all facets are displayed