Feature: Filter label is working


  @mineClassTest
  Scenario: Filter label is working
    Given user opens Home page
    When user applies Black color filter
    When user applies Blue color filter
    Then error message 'No results found' is displayed
    When user clear all facets
    Then 20 items are displayed on the page