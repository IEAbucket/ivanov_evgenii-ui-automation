package com.sandbox.pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Page Object of Home page
 */
public class HomePage {

    @FindBy(tagName = "ml-item-card")
    public List<WebElement> items;

    @FindBy(xpath = ".//ml-item-card//div[@class='brand']")
    public List<WebElement> itemsBrand;

    @FindBy(xpath = ".//input[contains(@class, 'search-by-text')]")
    public WebElement searchField;

    @FindBy(xpath = ".//div[contains(@class, 'clear-all')]")
    public WebElement clearAll;

    @FindBy(className = "no-results")
    public WebElement searchError;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='color_concept']")
    public WebElement colorBlock;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='occasion_concept']")
    public WebElement occasionBlock;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='material_concept']")
    public WebElement materialBlock;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='dress_length_concept']")
    public WebElement dressLengthBlock;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='dress_types_concept']")
    public WebElement dressTypesBlock;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='neckline_style_concept']")
    public WebElement necklineStyleBlock;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='theme_concept']")
    public WebElement themeBlock;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='attractability_concept']")
    public WebElement attractabilityBlock;

    @FindBy(xpath = ".//label[@title='Black']//span[@class='checkbox-mark']")
    public WebElement blackFilter;

    @FindBy(xpath = ".//label[@title='Blue']//span[@class='checkbox-mark']")
    public WebElement blueFilter;

    @FindBy(xpath = ".//ml-filter-group[@ng-reflect-title='brand_concept']")
    public WebElement brandBlock;

    @FindBy(xpath = ".//label[@title='Calvin Klein']//span[@class='checkbox-mark']")
    public WebElement calvinKleinFilter;

    @FindBy(xpath = ".//div[@class='active-filter']/span")
    public WebElement activeFilterLabelName;


    public void search(String text) {
        searchField.sendKeys(text);
        searchField.sendKeys(Keys.ENTER);
    }

    public void selectBlackFilter() {
        blackFilter.click();
    }
    public void selectBlueFilter() {
        blueFilter.click();
    }

    public void selectCalvinKleinFilter() {
        calvinKleinFilter.click();
    }

    public void clearAllFacets() {
        clearAll.click();
    }
}